const taskFunction = require('../task');

describe('taskFuntction', () => {
  it('should return array length', () => {
    expect(taskFunction([1, 2, 3])).to.equal(3);
  });

  it('should return sum of two numbers', () => {
    expect(taskFunction(1, 2)).to.equal(3);
  });
});